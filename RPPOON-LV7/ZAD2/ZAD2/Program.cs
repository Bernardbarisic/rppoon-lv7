﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 2, 7.3, 23, 1.2, -6, 4.75, -23, -67.6, -1, 24.2};
            NumberSequence numbersequence = new NumberSequence(array);
            LinearSort linearsort = new LinearSort();
            numbersequence.SetSearchStrategy(linearsort);
            int index = numbersequence.Search(23);
            if (index >= 0)
                Console.WriteLine("Number was found at: " + index);
            else
                Console.WriteLine("Number was not found");
        }
    }
}

