﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemdataprovider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemdataprovider.Attach(consoleLogger);
            while (true)
            {
                systemdataprovider.GetAvailableRAM();
                systemdataprovider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);

            }
        }
    }
}
