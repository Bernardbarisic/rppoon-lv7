﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            BubbleSort bubblesort = new BubbleSort();
            CombSort combsort = new CombSort();
            SequentialSort sequentialsort = new SequentialSort();
            double[] array = { 2, 7.3, 23, 1.2, -6, 4.75, -23, -67.6, -1, 24.2};

            Console.WriteLine("Bubblesort:");
            Console.WriteLine("\n");
            NumberSequence numbersequencebubble = new NumberSequence(array);
            numbersequencebubble.SetSortStrategy(bubblesort);
            Console.WriteLine("Before sort: \n" + numbersequencebubble.ToString());
            numbersequencebubble.Sort();
            Console.WriteLine("After sort: \n" + numbersequencebubble.ToString());
            Console.WriteLine("-------------------------------------------------");

            Console.WriteLine("Combsort:");
            Console.WriteLine("\n");
            NumberSequence numbersequencecomb = new NumberSequence(array);
            numbersequencecomb.SetSortStrategy(combsort);
            Console.WriteLine("Before sort: \n" + numbersequencecomb.ToString());
            numbersequencecomb.Sort();
            Console.WriteLine("After sort: \n" + numbersequencecomb.ToString());
            Console.WriteLine("-------------------------------------------------");

            Console.WriteLine("Sequentialsort:");
            Console.WriteLine("\n");
            NumberSequence numbersequencesequential = new NumberSequence(array);
            numbersequencesequential.SetSortStrategy(sequentialsort);
            Console.WriteLine("Before sort: \n" + numbersequencesequential.ToString());
            numbersequencesequential.Sort();
            Console.WriteLine("After sort: \n" + numbersequencesequential.ToString());
            Console.WriteLine("-------------------------------------------------");
        }
    }
}
